package com.onlyxiahui.general.doc.spring.boot.extend;

import java.util.List;

import com.onlyxiahui.general.doc.spring.boot.bean.TreeNode;

/**
 * Description <br>
 * Date 2020-06-01 11:04:05<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public interface DocLoadFinish {

	void finish(List<TreeNode> allList);
}
