package com.onlyxiahui.general.doc.spring.boot.extend;

import java.util.List;

import com.onlyxiahui.common.action.description.bean.ModuleData;

/**
 * Description <br>
 * Date 2020-06-02 09:27:40<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public interface DocActionHandler {

	void handle(List<ModuleData> list);
}
