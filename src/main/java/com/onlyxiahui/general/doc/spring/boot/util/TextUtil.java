package com.onlyxiahui.general.doc.spring.boot.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Description <br>
 * Date 2019-11-19 19:52:09<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class TextUtil {

	public static boolean maybeJsonArray(String string) {
		string = (null == string) ? string : string.trim();
		return string != null && ("null".equals(string) || (string.startsWith("[") && string.endsWith("]")));
	}

	public static String getTextByInputStream(InputStream input) {
		String text = null;
		if (null != input) {
			BufferedReader in = null;
			try {
				StringBuilder sb = new StringBuilder();
				in = new BufferedReader(new InputStreamReader(input));
				int i = 0;
				String temp;
				while ((temp = in.readLine()) != null) {
					if (i > 0) {
						sb.append("\n");
					}
					sb.append(temp);
					i++;
				}
				text = sb.toString();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (null != input) {
						input.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					if (null != in) {
						in.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return text;
	}
}
