package com.onlyxiahui.general.doc.spring.boot;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.onlyxiahui.common.action.description.DocumentService;

public class OnlyDocumentConfigurerInitializer {

	@Autowired
	DocumentService documentService;

	@Autowired(required = false)
	public void setConfigurers(List<OnlyDocumentConfigurer> configurers) {
		if (!CollectionUtils.isEmpty(configurers)) {
			for (OnlyDocumentConfigurer c : configurers) {
				if (null != documentService) {
					c.config(documentService.getDocumentContext());
				}
			}
		}
	}
}
