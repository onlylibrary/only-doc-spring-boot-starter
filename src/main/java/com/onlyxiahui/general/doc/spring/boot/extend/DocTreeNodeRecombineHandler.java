package com.onlyxiahui.general.doc.spring.boot.extend;

import java.util.List;
import java.util.Map;

import com.onlyxiahui.general.doc.spring.boot.bean.TreeNode;

/**
 * Description <br>
 * Date 2020-06-02 09:27:40<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public interface DocTreeNodeRecombineHandler {

	void handle(List<TreeNode> treeAllNodeList, List<TreeNode> treeRootNodeList, Map<String, TreeNode> allTreeMap);
}
