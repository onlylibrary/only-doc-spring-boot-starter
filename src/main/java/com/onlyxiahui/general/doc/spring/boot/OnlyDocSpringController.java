package com.onlyxiahui.general.doc.spring.boot;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.onlyxiahui.common.action.description.annotation.DocIgnore;
import com.onlyxiahui.general.doc.spring.boot.bean.ContentInfo;
import com.onlyxiahui.general.doc.spring.boot.service.OnlyDocTreeNodeService;

/**
 * 
 * 文档接口<br>
 * Date 2020-01-10 16:21:14<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 * @docIgnore
 */

@DocIgnore
@RequestMapping("/doc")
public class OnlyDocSpringController {

	@Autowired
	OnlyDocTreeNodeService onlyDocTreeNodeService;

	@ResponseBody
	@RequestMapping(value = "/index.tree.root")
	public List<?> indexRootList() {
		return onlyDocTreeNodeService.indexTreeRootList();
	}

	@ResponseBody
	@RequestMapping(value = "/index.get.by.key/{key}")
	public ContentInfo getIndexByKey(@PathVariable("key") String key) {
		return onlyDocTreeNodeService.getIndexByKey(key);
	}

	@ResponseBody
	@RequestMapping(value = "/index.list.by.text/{text}")
	public List<ContentInfo> indexListByText(@PathVariable("text") String text) {
		return onlyDocTreeNodeService.getIndexListByText(text);
	}

	/********************/

	@ResponseBody
	@RequestMapping(value = "/module.tree.root")
	public List<?> moduleRootList() {
		return onlyDocTreeNodeService.moduleTreeRootList();
	}

	@ResponseBody
	@RequestMapping(value = "/action.get.by.key/{key}")
	public ContentInfo getByKey(@PathVariable("key") String key) {
		return onlyDocTreeNodeService.getActionByKey(key);
	}

	@ResponseBody
	@RequestMapping(value = "/action.list.by.text/{text}")
	public List<ContentInfo> listByText(@PathVariable("text") String text) {
		return onlyDocTreeNodeService.getActionListByText(text);
	}

	@ResponseBody
	@GetMapping("/rebuild")
	public String rebuild() {
		onlyDocTreeNodeService.reload();
		return "成功";
	}

	@GetMapping("/index")
	public String index() {
		return "redirect:index.html";
	}

	@GetMapping
	public String load() {
		return "redirect:index.html";
	}
}
