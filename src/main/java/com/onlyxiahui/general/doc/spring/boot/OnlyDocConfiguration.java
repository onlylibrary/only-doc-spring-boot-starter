package com.onlyxiahui.general.doc.spring.boot;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.onlyxiahui.common.action.description.DocumentService;
import com.onlyxiahui.general.doc.spring.boot.service.OnlyDocTreeNodeService;

/**
 * 
 * Description <br>
 * Date 2020-01-10 16:21:01<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@org.springframework.context.annotation.Configuration
@EnableConfigurationProperties(OnlyDocProperties.class)
@ConditionalOnProperty(prefix = "only.doc", name = "enable", matchIfMissing = false)
public class OnlyDocConfiguration {

	@Bean
	public DocumentService documentService() {
		return new DocumentService();
	}

	@Bean
	public OnlyDocSpringController onlyDocSpringController() {
		return new OnlyDocSpringController();
	}

	@Bean
	public OnlyDocTreeNodeService onlyDocTreeNodeService() {
		return new OnlyDocTreeNodeService();
	}

	@Bean
	public OnlyDocumentConfigurerInitializer OnlyDocumentConfigurerInitializer() {
		return new OnlyDocumentConfigurerInitializer();
	}
}
