/**
 * Apache License Version 2.0, January 2004 http://www.apache.org/licenses/
 * 
 * TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
 * 
 * 1. Definitions.
 * 
 * "License" shall mean the terms and conditions for use, reproduction, and
 * distribution as defined by Sections 1 through 9 of this document.
 * 
 * "Licensor" shall mean the copyright owner or entity authorized by the
 * copyright owner that is granting the License.
 * 
 * "Legal Entity" shall mean the union of the acting entity and all other
 * entities that control, are controlled by, or are under common control with
 * that entity. For the purposes of this definition, "control" means (i) the
 * power, direct or indirect, to cause the direction or management of such
 * entity, whether by contract or otherwise, or (ii) ownership of fifty percent
 * (50%) or more of the outstanding shares, or (iii) beneficial ownership of
 * such entity.
 * 
 * "You" (or "Your") shall mean an individual or Legal Entity exercising
 * permissions granted by this License.
 * 
 * "Source" form shall mean the preferred form for making modifications,
 * including but not limited to software source code, documentation source, and
 * configuration files.
 * 
 * "Object" form shall mean any form resulting from mechanical transformation or
 * translation of a Source form, including but not limited to compiled object
 * code, generated documentation, and conversions to other media types.
 * 
 * "Work" shall mean the work of authorship, whether in Source or Object form,
 * made available under the License, as indicated by a copyright notice that is
 * included in or attached to the work (an example is provided in the Appendix
 * below).
 * 
 * "Derivative Works" shall mean any work, whether in Source or Object form,
 * that is based on (or derived from) the Work and for which the editorial
 * revisions, annotations, elaborations, or other modifications represent, as a
 * whole, an original work of authorship. For the purposes of this License,
 * Derivative Works shall not include works that remain separable from, or
 * merely link (or bind by name) to the interfaces of, the Work and Derivative
 * Works thereof.
 * 
 * "Contribution" shall mean any work of authorship, including the original
 * version of the Work and any modifications or additions to that Work or
 * Derivative Works thereof, that is intentionally submitted to Licensor for
 * inclusion in the Work by the copyright owner or by an individual or Legal
 * Entity authorized to submit on behalf of the copyright owner. For the
 * purposes of this definition, "submitted" means any form of electronic,
 * verbal, or written communication sent to the Licensor or its representatives,
 * including but not limited to communication on electronic mailing lists,
 * source code control systems, and issue tracking systems that are managed by,
 * or on behalf of, the Licensor for the purpose of discussing and improving the
 * Work, but excluding communication that is conspicuously marked or otherwise
 * designated in writing by the copyright owner as "Not a Contribution."
 * 
 * "Contributor" shall mean Licensor and any individual or Legal Entity on
 * behalf of whom a Contribution has been received by Licensor and subsequently
 * incorporated within the Work.
 * 
 * 2. Grant of Copyright License.
 * 
 * Subject to the terms and conditions of this License, each Contributor hereby
 * grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free,
 * irrevocable copyright license to reproduce, prepare Derivative Works of,
 * publicly display, publicly perform, sublicense, and distribute the Work and
 * such Derivative Works in Source or Object form.
 * 
 * 3. Grant of Patent License.
 * 
 * Subject to the terms and conditions of this License, each Contributor hereby
 * grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free,
 * irrevocable (except as stated in this section) patent license to make, have
 * made, use, offer to sell, sell, import, and otherwise transfer the Work,
 * where such license applies only to those patent claims licensable by such
 * Contributor that are necessarily infringed by their Contribution(s) alone or
 * by combination of their Contribution(s) with the Work to which such
 * Contribution(s) was submitted. If You institute patent litigation against any
 * entity (including a cross-claim or counterclaim in a lawsuit) alleging that
 * the Work or a Contribution incorporated within the Work constitutes direct or
 * contributory patent infringement, then any patent licenses granted to You
 * under this License for that Work shall terminate as of the date such
 * litigation is filed.
 * 
 * 4. Redistribution.
 * 
 * You may reproduce and distribute copies of the Work or Derivative Works
 * thereof in any medium, with or without modifications, and in Source or Object
 * form, provided that You meet the following conditions:
 * 
 * You must give any other recipients of the Work or Derivative Works a copy of
 * this License; and You must cause any modified files to carry prominent
 * notices stating that You changed the files; and You must retain, in the
 * Source form of any Derivative Works that You distribute, all copyright,
 * patent, trademark, and attribution notices from the Source form of the Work,
 * excluding those notices that do not pertain to any part of the Derivative
 * Works; and If the Work includes a "NOTICE" text file as part of its
 * distribution, then any Derivative Works that You distribute must include a
 * readable copy of the attribution notices contained within such NOTICE file,
 * excluding those notices that do not pertain to any part of the Derivative
 * Works, in at least one of the following places: within a NOTICE text file
 * distributed as part of the Derivative Works; within the Source form or
 * documentation, if provided along with the Derivative Works; or, within a
 * display generated by the Derivative Works, if and wherever such third-party
 * notices normally appear. The contents of the NOTICE file are for
 * informational purposes only and do not modify the License. You may add Your
 * own attribution notices within Derivative Works that You distribute,
 * alongside or as an addendum to the NOTICE text from the Work, provided that
 * such additional attribution notices cannot be construed as modifying the
 * License. You may add Your own copyright statement to Your modifications and
 * may provide additional or different license terms and conditions for use,
 * reproduction, or distribution of Your modifications, or for any such
 * Derivative Works as a whole, provided Your use, reproduction, and
 * distribution of the Work otherwise complies with the conditions stated in
 * this License.
 * 
 * 5. Submission of Contributions.
 * 
 * Unless You explicitly state otherwise, any Contribution intentionally
 * submitted for inclusion in the Work by You to the Licensor shall be under the
 * terms and conditions of this License, without any additional terms or
 * conditions. Notwithstanding the above, nothing herein shall supersede or
 * modify the terms of any separate license agreement you may have executed with
 * Licensor regarding such Contributions.
 * 
 * 6. Trademarks.
 * 
 * This License does not grant permission to use the trade names, trademarks,
 * service marks, or product names of the Licensor, except as required for
 * reasonable and customary use in describing the origin of the Work and
 * reproducing the content of the NOTICE file.
 * 
 * 7. Disclaimer of Warranty.
 * 
 * Unless required by applicable law or agreed to in writing, Licensor provides
 * the Work (and each Contributor provides its Contributions) on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied, including, without limitation, any warranties or conditions of
 * TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR
 * PURPOSE. You are solely responsible for determining the appropriateness of
 * using or redistributing the Work and assume any risks associated with Your
 * exercise of permissions under this License.
 * 
 * 8. Limitation of Liability.
 * 
 * In no event and under no legal theory, whether in tort (including
 * negligence), contract, or otherwise, unless required by applicable law (such
 * as deliberate and grossly negligent acts) or agreed to in writing, shall any
 * Contributor be liable to You for damages, including any direct, indirect,
 * special, incidental, or consequential damages of any character arising as a
 * result of this License or out of the use or inability to use the Work
 * (including but not limited to damages for loss of goodwill, work stoppage,
 * computer failure or malfunction, or any and all other commercial damages or
 * losses), even if such Contributor has been advised of the possibility of such
 * damages.
 * 
 * 9. Accepting Warranty or Additional Liability.
 * 
 * While redistributing the Work or Derivative Works thereof, You may choose to
 * offer, and charge a fee for, acceptance of support, warranty, indemnity, or
 * other liability obligations and/or rights consistent with this License.
 * However, in accepting such obligations, You may act only on Your own behalf
 * and on Your sole responsibility, not on behalf of any other Contributor, and
 * only if You agree to indemnify, defend, and hold each Contributor harmless
 * for any liability incurred by, or claims asserted against, such Contributor
 * by reason of your accepting any such warranty or additional liability.
 * 
 * END OF TERMS AND CONDITIONS
 * 
 * APPENDIX: How to apply the Apache License to your work
 * 
 * To apply the Apache License to your work, attach the following boilerplate
 * notice, with the fields enclosed by brackets "{}" replaced with your own
 * identifying information. (Don't include the brackets!) The text should be
 * enclosed in the appropriate comment syntax for the file format. We also
 * recommend that a file or class name and description of purpose be included on
 * the same "printed page" as the copyright notice for easier identification
 * within third-party archives.
 * 
 * Copyright 2019 XiaHui[onlovexiahui@qq.com]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 **/

package com.onlyxiahui.general.doc.spring.boot.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.onlyxiahui.common.action.description.DocumentService;
import com.onlyxiahui.common.action.description.bean.MethodData;
import com.onlyxiahui.common.action.description.bean.ModuleData;
import com.onlyxiahui.common.action.description.format.bean.ActionData;
import com.onlyxiahui.common.action.markdown.MarkdownBuilder;
import com.onlyxiahui.general.doc.spring.boot.OnlyDocProperties;
import com.onlyxiahui.general.doc.spring.boot.bean.ContentInfo;
import com.onlyxiahui.general.doc.spring.boot.bean.IndexData;
import com.onlyxiahui.general.doc.spring.boot.bean.TreeNode;
import com.onlyxiahui.general.doc.spring.boot.extend.DocActionFinish;
import com.onlyxiahui.general.doc.spring.boot.extend.DocActionHandler;
import com.onlyxiahui.general.doc.spring.boot.extend.DocLoadFinish;
import com.onlyxiahui.general.doc.spring.boot.extend.DocTreeNodeRecombineHandler;
import com.onlyxiahui.general.doc.spring.boot.extend.DocTreeNodeSort;
import com.onlyxiahui.general.doc.spring.boot.manager.OnlyDocContentInfoManager;
import com.onlyxiahui.general.doc.spring.boot.manager.OnlyDocTreeNodeManager;
import com.onlyxiahui.general.doc.spring.boot.util.DocAopTargetUtils;
import com.onlyxiahui.general.doc.spring.boot.util.TextUtil;

/**
 * Description <br>
 * Date 2020-01-11 19:50:09<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class OnlyDocTreeNodeService implements ApplicationContextAware {

	@Autowired
	OnlyDocProperties onlyDocProperties;

	@Autowired
	DocumentService documentService;

	OnlyDocContentInfoManager actionInfoManager = new OnlyDocContentInfoManager();
	OnlyDocTreeNodeManager actionTreeNodeManager = new OnlyDocTreeNodeManager();

	OnlyDocContentInfoManager indexInfoManager = new OnlyDocContentInfoManager();
	OnlyDocTreeNodeManager indexTreeNodeManager = new OnlyDocTreeNodeManager();

	List<BeanFactory> beanFactorys;
	MarkdownBuilder mb = new MarkdownBuilder();

	boolean isLoad = false;
	Boolean onLoading = false;

	@Autowired(required = false)
	public void setConfigurers(List<BeanFactory> beanFactorys) {
		this.beanFactorys = beanFactorys;
	}

	List<DocActionFinish> docActionFinishes;

	@Autowired(required = false)
	public void setDocActionFinishes(List<DocActionFinish> docActionFinishes) {
		this.docActionFinishes = docActionFinishes;
	}

	List<DocLoadFinish> docLoadFinishes;

	@Autowired(required = false)
	public void setDocLoadFinishes(List<DocLoadFinish> docLoadFinishes) {
		this.docLoadFinishes = docLoadFinishes;
	}

	@PostConstruct
	public void initialize() {
		// OnlyPathClassScanner pcs = new OnlyPathClassScanner();
		// documentService.getDocumentContext().setDocClassScanner(pcs);
		// 使用多线程异步去初始化,尽量不阻塞系统启动
		reload();
	}

	@Autowired(required = false)
	private DocTreeNodeSort docTreeNodeSort;

	@Autowired(required = false)
	private DocTreeNodeRecombineHandler docTreeNodeRecombineHandler;

	@Autowired(required = false)
	private DocActionHandler docActionHandler;

	public void reload() {
		if (!onLoading) {
			onLoading = true;
			Thread thread = new Thread() {
				@Override
				public void run() {
					try {
						load();
					} finally {
						onLoading = false;
					}
				}
			};
			thread.start();
		}
	}

	public void load() {
		clear();
		loadIndex();
		loadAction();
	}

	public void loadAction() {
		Set<String> sourcePaths = new HashSet<>();
		sourcePaths.add("classpath*:**/*.java");
		sourcePaths.add("file:src/main/java/**/*.java");

		Set<String> sources = onlyDocProperties.getSourcePaths();
		if (null != sources) {
			sourcePaths.addAll(sources);
		}
		boolean loadSource = onlyDocProperties.isLoadSource();

		if (loadSource) {
			for (String path : sourcePaths) {
				documentService.loadSource(path);
			}
		}

		Set<String> packagePaths = new HashSet<>();
		if (!CollectionUtils.isEmpty(beanFactorys)) {
			for (BeanFactory beanFactory : beanFactorys) {
				if (AutoConfigurationPackages.has(beanFactory)) {
					List<String> packagesToScan = AutoConfigurationPackages.get(beanFactory);
					if (null != packagesToScan) {
						packagePaths.addAll(packagesToScan);
					}
				}
			}
		}

		if (null != applicationContext) {
			applicationContext.getBeansWithAnnotation(ComponentScan.class).forEach((name, instance) -> {
				Object o = null;
				try {
					o = DocAopTargetUtils.getTarget(instance);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (null != o) {
					Set<ComponentScan> scans = AnnotatedElementUtils.getMergedRepeatableAnnotations(o.getClass(), ComponentScan.class);
					for (ComponentScan scan : scans) {
						String[] bs = scan.basePackages();
						String[] vs = scan.value();
						if (null != bs) {
							for (String ps : bs) {
								packagePaths.add(ps);
							}
						}
						if (null != vs) {
							for (String ps : vs) {
								packagePaths.add(ps);
							}
						}
					}
				}
			});
		}
		Set<String> packages = onlyDocProperties.getPackagePaths();
		if (null != packages) {
			packagePaths.addAll(packages);
		}
		if (packagePaths.isEmpty()) {
			packagePaths.add("com");
		}

		List<ModuleData> allList = new ArrayList<>();
		for (String path : packagePaths) {
			List<ModuleData> list = documentService.scanPackages(path);
			if (null != list) {
				allList.addAll(list);
			}
		}

		if (null != docActionHandler) {
			docActionHandler.handle(allList);
		}

		loadAction(allList);

		if (null != docActionFinishes && !docActionFinishes.isEmpty()) {
			if (null != docActionFinishes && !docActionFinishes.isEmpty()) {
				for (DocActionFinish f : docActionFinishes) {
					f.finish(allList);
				}
			}
		}

		actionTreeNodeManager.recombine(docTreeNodeSort, docTreeNodeRecombineHandler);

		if (null != docLoadFinishes && !docLoadFinishes.isEmpty()) {
			for (DocLoadFinish f : docLoadFinishes) {
				f.finish(actionTreeNodeManager.allList());
			}
		}

		isLoad = true;
	}

	public void loadAction(List<ModuleData> list) {
		for (ModuleData md : list) {
			if (md.isIgnore()) {
				continue;
			}
			String superKey = md.getKey();
			add(md);
			List<ActionData> actionList = mb.formatting(md);
			for (ActionData action : actionList) {
				MethodData methodData = action.getMethodData();
				String key = methodData.getKey();
				String title = methodData.getTitle();
				Set<String> paths = methodData.getActions();
				String path = (paths == null || paths.isEmpty()) ? null : paths.iterator().next();
				actionTreeNodeManager.add(key, superKey, title, path, true);
				addActionInfo(key, superKey, title, action.getContent());
			}
		}
	}

	public void add(ModuleData md) {
		String superKey = md.getSuperKey();
		String key = md.getKey();
		String title = md.getTitle();
		List<String> paths = md.getPaths();
		String path = (paths == null || paths.isEmpty()) ? null : paths.get(0);
		boolean action = false;
		actionTreeNodeManager.add(key, superKey, title, path, action);
	}

	public boolean isLoad() {
		return isLoad;
	}

	public void clear() {
		documentService.clear();

		actionInfoManager.clear();
		actionTreeNodeManager.clear();

		indexInfoManager.clear();
		indexTreeNodeManager.clear();
	}

	public void addActionInfo(String key, String moduleKey, String title, String content) {
		actionInfoManager.add(key, moduleKey, title, content);
	}

	/*******************************************/

	public ContentInfo getActionByKey(String key) {
		return actionInfoManager.getByKey(key);
	}

	public List<ContentInfo> getActionListByText(String text) {
		return actionInfoManager.listByText(text);
	}

	public List<ContentInfo> getAllActionList() {
		return actionInfoManager.allList();
	}

	public List<TreeNode> moduleTreeRootList() {
		return actionTreeNodeManager.treeRootList();
	}

	/*******************************************/
	public void loadIndex() {
		String path = onlyDocProperties.getIndexeConfig();
		List<String> list = getTextList(path);
		if (!list.isEmpty()) {
			String json = list.get(0);
			if (TextUtil.maybeJsonArray(json)) {
				List<IndexData> indexes = JSONObject.parseObject(json, new TypeReference<List<IndexData>>() {
				});
				if (null != indexes) {
					for (IndexData index : indexes) {
						loadIndex(index);
					}
				}
			}
		}
		indexTreeNodeManager.recombine(docTreeNodeSort, docTreeNodeRecombineHandler);
	}

	public void loadIndex(IndexData index) {
		if (null != index) {
			String resourcePath = index.getPath();
			StringBuilder sb = new StringBuilder();
			List<String> list = getTextList(resourcePath);
			int length = list.size();
			int last = length - 1;
			for (int i = 0; i < length; i++) {
				String text = list.get(i);
				if (null != text && !text.isEmpty()) {
					sb.append(text);
					if (i < last) {
						sb.append("\n");
					}
				}
			}
			String key = index.getKey();
			String superKey = index.getSuperKey();
			String title = index.getTitle();
			String path = null;

			String content = sb.toString();
			boolean action = !content.isEmpty();

			indexTreeNodeManager.add(key, superKey, title, path, action);
			indexInfoManager.add(key, superKey, title, content);
		}
	}

	public List<String> getTextList(String resourcePath) {

		List<String> list = new ArrayList<>();
		if (null != resourcePath && !resourcePath.isEmpty()) {
			PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
			Resource[] resources = null;
			try {
				resources = resolver.getResources(resourcePath);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			if (resources != null) {
				int length = resources.length;
				for (int i = 0; i < length; i++) {
					Resource resource = resources[i];
					InputStream input = null;
					try {
						input = resource.getInputStream();
					} catch (IOException e) {
						e.printStackTrace();
						throw new RuntimeException(e);
					}

					if (null != input) {
						String text = TextUtil.getTextByInputStream(input);
						if (null != text && !text.isEmpty()) {
							list.add(text);
						}
					}
				}
			}
		}
		return list;
	}

	public ContentInfo getIndexByKey(String key) {
		return indexInfoManager.getByKey(key);
	}

	public List<ContentInfo> getIndexListByText(String text) {
		return indexInfoManager.listByText(text);
	}

	public List<TreeNode> indexTreeRootList() {
		return indexTreeNodeManager.treeRootList();
	}

	private ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
