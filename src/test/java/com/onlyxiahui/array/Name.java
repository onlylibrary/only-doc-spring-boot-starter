package com.onlyxiahui.array;

/**
 * Description <br>
 * Date 2020-01-08 18:21:01<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class Name {
	String[][][] a = { { { "yyy" } }, { { "xxx" } } };

	public String[][][] getA() {
		return a;
	}

	public void setA(String[][][] a) {
		this.a = a;
	}

}
