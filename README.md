# only-doc-spring-boot-starter

#### 介绍
自动spring boot 接口文档

这是通过源码扫描生成接口文档的工具

### 使用方式
1、maven 引入依赖
```
		<dependency>
			<groupId>com.onlyxiahui.general</groupId>
			<artifactId>only-doc-spring-boot-starter</artifactId>
			<version>1.0.0</version>
		</dependency>
```

2、需要生成文档的接口需要源码


```
开发工具中运行通常会自动扫描到源码
但打编译打包后jar是不包含源码，如果希望打包后运行有文档，需要把源码打包进jar
然后对于第三方依赖，可以用maven引入源码<classifier>sources</classifier>
```


```
1)源码打包进jar，需要插件配置
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>3.1.0</version>
				<executions>
					<execution>
						<id>copy-resources</id>
						<phase>process-resources</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>${project.build.outputDirectory}
							</outputDirectory>
							<resources>
								<resource>
									<directory>src/main/java</directory>
									<includes>
										<include>**/*.java</include>
									</includes>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
		
2)引入的jar导入源码
<!--依赖库源码-->
<dependency>
	<groupId>org.springframework</groupId>
	<artifactId>spring-core</artifactId>
    <version>5.0.0.RELEASE</version>
    <classifier>sources</classifier>
    <!--设置为test,项目发布时source不会放入最终的产品包-->
    <scope>test</scope>
</dependency>

```

3、打开接口文档
目前默认为http://xxx.xxx.xxx.xxx:xx/doc/index.html

4、配置文件
```
#默认情况下是无需配置，如果出现不能出文档的情况，可以尝试以下配置

#公共首页配置
only.doc.indexes[0].key=0
only.doc.indexes[0].superKey=
only.doc.indexes[0].title=全局说明
#markdown格式文件地址
only.doc.indexes[0].path=

# 是否开启
only.doc.enable=false
#标题
only.doc.title=接口文档
#版本
only.doc.version=1.0.0
#接口根目录
only.doc.basePath=/
#源码路径1 classpath下的配置
only.doc.source-paths[0]=classpath*:**/*.java
#源码路径2 相对路径或者绝对路径
only.doc.source-paths[1]=file:src/main/java/**/*.java
#接口包目录1
only.doc.package-paths[0]=com.dome
#接口包目录2
only.doc.package-paths[1]=com.api
```
